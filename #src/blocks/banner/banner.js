bannerInit();
$(window).on('resize', function() {
	bannerInit();
});

function bannerInit() {
	if ( $('.section .banner-js').length ) {
		if ( $(window).width() >= 992 ) {
			
			$('.banner').each(function() {
				var banH = $(this).height();

				$(this).css('margin-bottom', - banH / 2);

				$(this).closest('.section')
					.css('margin-bottom', banH / 2 + 77);
			});

		}
	}
}