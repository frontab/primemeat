categoryListInit();
$(window).on('resize', function() {
	// categoryListInit();
});
function categoryListInit() {
	if ( $('.category__list').length ) {
		if ( $(window).width() < 992 ) {
			$('.category__list').scrollbar();
		} else {
			$('.category__list').scrollbar('destroy');
		}
	}
}