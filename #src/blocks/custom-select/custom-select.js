$('.custom-select').each(function() {
	customSelectInit( $(this) );
});

function customSelectInit( select ) {}

$(document).on('click', '.custom-select__select', function(e) {
	e.preventDefault();

	$(this).closest('.custom-select')
		.toggleClass('active');
});

$(document).on('click', '.custom-select__dropdown li', function(e) {
	e.preventDefault();

	$(this).addClass('selected')
		.siblings()
		.removeClass('selected');

	$(this).closest('.custom-select')
		.removeClass('active')
		.find('.custom-select__text')
		.text( $(this).text() );
});

$(document).mouseup(function (e) {
	var boxArea = $('.custom-select');
	if (boxArea.has(e.target).length === 0) {
		boxArea.removeClass('active');
	}
});