$(document).on('click', '.tabs__btn', function(e) {
	e.preventDefault();

	$(this).addClass('active')
		.siblings()
		.removeClass('active');

	var itemIndex = $(this).index();

	$('.tabs__box').each(function() {
		if ( $(this).index() === itemIndex ) {
			$(this).addClass('active')
				.siblings()
				.removeClass('active');
		}
	});
});


$(document).on('click', '.tabs__box-btn', function(e) {
	e.preventDefault();

	$(this).toggleClass('active')
		.closest('.tabs__box')
		.siblings()
		.find('.tabs__box-btn')
		.removeClass('active');

	if ( $(window).scrollTop() + 56 > $(this).offset().top ) {
		$('html,body').animate({scrollTop: $(this).offset().top - 56}, 300);
	}
});
