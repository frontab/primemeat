$(document).on('click', '.title--basker-trigger:not(.simple)', function(e) {
	e.preventDefault();

	var newVal = $('.form--checkout').offset().top - 46 - 47;

	$('html,body').animate({scrollTop: newVal}, 300);
});

basketBtnCheck();
$(window).on('scroll', function() {
	basketBtnCheck();
});

function basketBtnCheck() {
	if ( $('.title--basker-trigger').length ) {
		if ( $(window).width() < 768 ) {

			var wBottom = $(window).scrollTop() + $(window).height(),
				btnPosBottom = $('.form--checkout').offset().top;

				console.log(`Экран: ${wBottom}`);
				console.log(`Кнопка: ${btnPosBottom}`);

			if ( wBottom > btnPosBottom ) {
				$('.title--basker-trigger').addClass('simple');
				$('.basket-page').addClass('checkout-open');
			} else {
				$('.title--basker-trigger').removeClass('simple');
				$('.basket-page').removeClass('checkout-open');
			}
		}
	}
}