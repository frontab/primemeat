$('.filter-box__content').scrollbar();

$(document).on('click', '.filter-box__title', function(e) {

	if ( $(window).width() < 991 ) {
		e.preventDefault();

		$(this).closest('.filter-box')
			.toggleClass('active');
	}
});

$(document).on('click', '.filter-top__reset', function() {
	$(this).closest('.filter')
		.find('.filter-box')
		.removeClass('edit');
});

$(document).on('click', '.filter-box', function() {
	filterCheck($(this));
});

function filterCheck(box) {
	box.find('.form__check')
		.each(function() {
			if ( $(this).prop('checked') ) {
				$(this).closest('.filter-box')
					.addClass('edit');
					return false;
			} else {
				$(this).closest('.filter-box')
					.removeClass('edit');
			}
		});
}