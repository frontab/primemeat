$(document).on('click', '.card-alt__btn .btn', function(e) {
	e.preventDefault();

	$(this).closest('.card-alt')
		.addClass('active');
});

cardsAltInit();
$(window).on('resize', function() {
	// cardsAltInit();
});
function cardsAltInit() {
	if ( $('.cards-alt').length ) {
		if ( $(window).width() < 992 ) {
			$('.cards-alt').scrollbar();
		} else {
			$('.cards-alt').scrollbar('destroy');
		}
	}
}