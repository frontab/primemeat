$(document).on('click', '.card__btn', function(e) {
	e.preventDefault();

	$(this).closest('.card')
		.addClass('added');
});


$(document).on('click', '.card__tags-btn', function(e) {
	e.preventDefault();

	$(this).closest('.card__tags')
		.toggleClass('active');
});


$(document).on('click', '.card .counter a', function() {

	var price = parseInt( $(this).closest('.card')
		.find('.card__price')
		.attr('data-price')
		.replace(' ','') );

	var area = $(this).closest('.card')
		.find('.card__price span');

	counterPriceEdit($(this), price, area);
});

function counterPriceEdit(btn, price, area) {

	var num = parseInt( btn.siblings('.counter__line').find('span').text() );
	var val = price * num + "";
	var newVal = val.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');

	area.text( newVal );
}