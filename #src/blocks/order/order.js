orderStepsInit();
$(window).on('resize', function() {
	// orderStepsInit();
});
function orderStepsInit() {
	if ( $('.order-steps').length ) {
		if ( $(window).width() >= 992 ) {
			$('.order-steps').scrollbar('destroy');
		} else {
			$('.order-steps').scrollbar();
		}
	}
}