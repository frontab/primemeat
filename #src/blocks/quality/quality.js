qualityItemsInit();
$(window).on('resize', function() {
	// qualityItemsInit();
});
function qualityItemsInit() {
	if ( $('.quality__items').length ) {
		if ( $(window).width() < 992 ) {
			$('.quality__items').scrollbar();
		} else {
			$('.quality__items').scrollbar('destroy');
		}
	}
}

qualityListInit();
$(window).on('resize', function() {
	// qualityListInit();
});
function qualityListInit() {
	if ( $('.quality-block__list').length ) {
		if ( $(window).width() < 992 ) {
			$('.quality-block__list').scrollbar();
		} else {
			$('.quality-block__list').scrollbar('destroy');
		}
	}
}


qualityContInit();
$(window).on('resize', function() {
	// qualityContInit();
});
function qualityContInit() {
	if ( $('.quality-cont').length ) {
		if ( $(window).width() < 992 ) {
			$('.quality-cont').scrollbar();
		} else {
			$('.quality-cont').scrollbar('destroy');
		}
	}
}
