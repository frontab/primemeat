$('.main-slider').slick({
	arrows: true,
	prevArrow: '<button type="button" class="slick-prev"><i class="icon icon-angle-left-alt"</i></button>',
	nextArrow: '<button type="button" class="slick-next"><i class="icon icon-angle-right-alt"</i></button>',
	dots: true,
	infinite: true,
	centerMode: true,
	variableWidth: true,
	slidesToShow: 1,
	slidesToScroll: 1
});