

$(document).on('click', '.share', function(e) {
	e.preventDefault();

	$(this).addClass('active');
});


$(document).mouseup(function (e) {
	var boxArea = $('.share-drop');
	if (boxArea.has(e.target).length === 0) {
		boxArea.prev('.share')
			.removeClass('active');
	}
});