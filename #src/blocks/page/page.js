$(document).on('click', '.page-controls__btns button', function(e) {
	e.preventDefault();

	$(this).addClass('active')
		.siblings()
		.removeClass('active');

	var bthType = $(this).attr("data-type");

	if ( bthType === "lines" ) {
		$('.area-type').addClass('isLine');
	} else {
		$('.area-type').removeClass('isLine');
	}
});


$(document).on('click', '.page-top__link', function(e) {
	e.preventDefault();

	$(this).addClass('active')
		.closest('li')
		.siblings()
		.find('.page-top__link')
		.removeClass('active');
});

pageTabsCheck();
$(window).on('resize', function() {
	pageTabsCheck();
});
function pageTabsCheck() {
	if ( $(window).width() < 768 ) {
		if ( !$('.basket-page').hasClass('checkout-open') ) {

			$('.page-tabs__btn').removeClass('active');
			$('.page-tabs__box').removeClass('active');
		}
	}
}

$(document).on('click', '.page-tabs__btn', function(e) {
	e.preventDefault();

	if ( $(window).width() < 768 ) {
		$(this).toggleClass('active')
			.addClass('selected')
			.siblings()
			.removeClass('active selected');
		var btnIndex = $(this).index();
	
		$(this).closest('.page-tabs')
			.find('.page-tabs__box')
			.each(function() {
				if ( $(this).index() === btnIndex ) {
					$(this).toggleClass('active')
						.siblings()
						.removeClass('active');
				}
			});
	} else {
		$(this).addClass('active')
			.addClass('selected')
			.siblings()
			.removeClass('active selected');
		var btnIndex = $(this).index();
	
		$(this).closest('.page-tabs')
			.find('.page-tabs__box')
			.each(function() {
				if ( $(this).index() === btnIndex ) {
					$(this).addClass('active')
						.siblings()
						.removeClass('active');
				}
			});
	}
});



$(document).on('click', '.page-text-trigger button', function(e) {
	e.preventDefault();

	$(this).closest('.page-text-trigger')
		.toggleClass('active');
});



$(document).ready(function() {
	Tipped.create('.js-tooltip',{
		position: 'bottom',
		skin: 'light'
	});
 });