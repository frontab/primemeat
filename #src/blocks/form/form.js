$('.form__select').styler();


inputCheck();
function inputCheck() {
	if ( $('.form__label--comb .form__input').length ) {

		$('.form__label--comb .form__input').each(function() {
			if ( $(this).val().length ) {
				$(this).closest('.form__label--comb')
					.addClass('edit');
			} else {
				$(this).closest('.form__label--comb')
					.removeClass('edit');
			}
		});
	}
}

$('.form__label--comb .form__input').on('input change', function(e) {

	if ( $(this).val().length ) {
		$(this).closest('.form__label--comb')
			.addClass('edit');
	} else {
		$(this).closest('.form__label--comb')
			.removeClass('edit');
	}
});

$('.form__date').datepicker({
	minDate: new Date(),
	dateFormat: "yy-mm-dd"
});

$('.form-date').datepicker({
	minDate: new Date(),
	dateFormat: "yy-mm-dd"
});