$(document).on('click', '.deliv-box__trigger', function(e) {
	e.preventDefault();

	if ( $(window).width() < 992 ) {
		$(this).closest('.deliv-box')
			.toggleClass('active');
	}
});