$(document).on('click', '.section-btn', function(e) {
	e.preventDefault();

	var slider = $(this).closest('.section')
		.find('.slick-slider');

	if ($(this).hasClass('section-btn__left')) {
		slider.slick('slickPrev');
	} else if ($(this).hasClass('section-btn__right')) {
		slider.slick('slickNext');
	}
});