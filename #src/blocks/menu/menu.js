$(document).on('click', '.js-menu-link', function(e) {
	if ( $(window).width() < 1200 ) {
		e.preventDefault();

		$(this).toggleClass("active")
			.closest('li')
			.siblings()
			.find('.js-menu-link')
			.removeClass('active');
	}
});

$(document).on('mouseover', '.js-menu-link', function(e) {
	if ( $(window).width() >= 1200 ) {
		$(this).toggleClass("active")
			.closest('li')
			.siblings()
			.find('.js-menu-link')
			.removeClass('active');
	}
});


$(document).on('click', '.menu-btn', function(e) {
	e.preventDefault();

	$(this).toggleClass('active');
});

$(document).mouseup(function (e) {
	var boxArea = $('.header__menu');
	if (boxArea.has(e.target).length === 0) {
		boxArea.find('.menu-btn')
			.removeClass('active');
	}
});

$('.menu-drop').scrollbar();