// $('.card-slider').slick({
// 	arrows: true,
// 	prevArrow: '<button type="button" class="slick-prev"><i class="icon icon-angle-left-alt"</i></button>',
// 	nextArrow: '<button type="button" class="slick-next"><i class="icon icon-angle-right-alt"</i></button>',	
// 	dots: false,
// 	infinite: true,
// 	slidesToShow: 4,
// 	slidesToScroll: 1,
// 	swipeToSlide: true
// });


// swipeToSlide: true,
// autoSlidesToShow: true,
// variableWidth: true

cardSliderInit();
$(window).on('resize', function() {
	// cardSliderInit();
});
function cardSliderInit() {
	if ( $('.card-slider').length ) {
		if ( $(window).width() >= 992 ) {
			$('.card-slider').slick({
				arrows: true,
				prevArrow: '<button type="button" class="slick-prev"><i class="icon icon-angle-left-alt"</i></button>',
				nextArrow: '<button type="button" class="slick-next"><i class="icon icon-angle-right-alt"</i></button>',	
				dots: false,
				infinite: true,
				slidesToShow: 4,
				slidesToScroll: 1,
				swipeToSlide: true
			});
		} else {
			$('.card-slider').scrollbar();
		}
	}
}