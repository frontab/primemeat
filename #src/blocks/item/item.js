$(document).on('click', '.item-tabs__btn', function(e) {
	e.preventDefault();

	$(this).addClass('active')
		.siblings()
		.removeClass('active');

	var itemIndex = $(this).index();

	$('.item-tabs__box').each(function() {
		if ( $(this).index() === itemIndex ) {
			$(this).addClass('active')
				.siblings()
				.removeClass('active');
		}
	});
});

$(document).on('click', '.item-tabs__box-btn', function(e) {
	e.preventDefault();

	$(this).toggleClass('active')
		.closest('.item-tabs__box')
		.siblings()
		.find('.item-tabs__box-btn')
		.removeClass('active');

	if ( $(window).scrollTop() + 56 > $(this).offset().top ) {
		$('html,body').animate({scrollTop: $(this).offset().top - 56}, 300);
	}
});

$('.item__scroll').scrollbar();

$('.item__imgs').slick({
	arrows: true,
	prevArrow: '<button type="button" class="slick-prev"><i class="icon icon-angle-left"</i></button>',
	nextArrow: '<button type="button" class="slick-next"><i class="icon icon-angle-right"</i></button>',
	dots: true,
	infinite: true,
	slidesToShow: 1,
	slidesToScroll: 1,
	customPaging: function(slick, index) {
	  var image = $(slick.$slides[index]).find('img').attr('src');
	  return '<img src="' + image + '" alt="" /> '
	},
	responsive: [
		{
			breakpoint: 1199,
			settings: {
				prevArrow: '<button type="button" class="slick-prev"><i class="icon icon-angle-left-alt"</i></button>',
				nextArrow: '<button type="button" class="slick-next"><i class="icon icon-angle-right-alt"</i></button>',			
			}
		}
	 ]
});