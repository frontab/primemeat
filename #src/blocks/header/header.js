headerMove();
$(window).on('scroll', function() {
	headerMove();
});
function headerMove() {
	if ( $(window).width() >= 1200 ) {
		var wTop = $(window).scrollTop(),
			minVal = 70,
			maxVal = 135,
			newHeight = maxVal - wTop,
			newBot = 10 - wTop / 4;

		if ( newHeight < minVal ) {
			newHeight = minVal;
		}

		if ( newHeight > maxVal ) {
			newHeight = maxVal;
		}

		if ( newBot < 0 ) {
			newBot = 0;
		}

		if ( newBot > 10 ) {
			newBot = 10;
		}

		$('.header__top').css({
			'height': newHeight,
			"padding-bottom": newBot
		});

		var logoWMin = 97,
			logoWMax = 163,
			newWidth = logoWMax - wTop;

		if ( newWidth < logoWMin ) {
			newWidth = logoWMin;
		}

		if ( newWidth > logoWMax ) {
			newWidth = logoWMax;
		}

		$('.header__logo img').css('width',newWidth);

		var spanHMin = 0,
			spanHMax = 15,
			spanNewHeight = spanHMax - wTop / 4,
			spanNewSize = spanHMax - wTop / 2.5,
			newOpVal = (100 - 6 * wTop) * 0.01;

		if ( spanNewHeight < spanHMin ) {
			spanNewHeight = spanHMin;
		}

		if ( spanNewHeight > spanHMax ) {
			spanNewHeight = spanHMax;
		}

		if ( spanNewSize < spanHMin ) {
			spanNewSize = spanHMin;
		}

		if ( spanNewSize > spanHMax ) {
			spanNewSize = spanHMax;
		}

		if ( newOpVal < 0 ) {
			newOpVal = 0;
		}

		if ( newOpVal > 1 ) {
			newOpVal = 1;
		}

		$('.header__logo span').css({
			"font-size": spanNewSize,
			"opacity": newOpVal,
			"height": spanNewHeight
		});
	}
}


$(document).on('click', '.js-menu-trigger', function(e) {
	e.preventDefault();

	$('body').toggleClass('mob-menu-opened');
});

$(document).on('click', '.header-mobile__btn--search .icon', function(e) {
	e.preventDefault();

	$(this).closest('.header-mobile__btn--search')
		.toggleClass('active');
});

$(document).mouseup(function (e) {
	var boxArea = $('.header-mobile__btn--search');
	if (boxArea.has(e.target).length === 0) {
		boxArea.removeClass('active');
	}
});